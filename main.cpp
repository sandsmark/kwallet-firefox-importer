#include "dialog.h"
#include <KApplication>
#include <KAboutData>
#include <KCmdLineArgs>


static KAboutData about(
    "FirefoxImporter",
    "",
    ki18n("FirefoxImporter"),
    "0.1",
    ki18n("Imports passwords from Firefox to KWallet."),
    KAboutData::License_LGPL,
    ki18n("(C) 2010 Martin Sandsmark"),
    KLocalizedString(),
    "http://www.mts.ms/");

int main(int argc, char *argv[])
{
    about.addAuthor(ki18n("Martin Sandsmark"), ki18n("Maintainer"), "sandsmark@iskrembilen.com", "http://iskrembilen.com/");

    KCmdLineArgs::init(argc, argv, &about);
    KApplication app;

    Dialog dialog;
    dialog.show();
    return app.exec();
}

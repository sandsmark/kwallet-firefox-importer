#ifndef IMPORTER_H
#define IMPORTER_H

#include <secmodt.h>
#include <QList>
#include <QObject>

#include "login.h"

class Importer : public QObject
{
    Q_OBJECT

public:
    Importer();
    ~Importer();
    QList<Login> fetchLogins();

private:
    QString decrypt(QString);
    QByteArray m_profilePath;
    PK11SlotInfo *m_slot;
};

#endif//IMPORTER_H

project(Importer)

find_package(KDE4 REQUIRED)
# We use Mozillas stuff, because I don't want to reimplement db2
find_package(QCA2 REQUIRED)
add_definitions(${QT_DEFINITIONS} ${KDE4_DEFINITIONS} ${QCA2_DEFINITIONS})
include(KDE4Defaults)
include(MacroLibrary)
include_directories(${KDE4_INCLUDES} ${KDE4_INCLUDE_DIR} ${QT_INCLUDES} ${QCA2_INCLUDES} /usr/include/nss /usr/include/nspr)

set(SRCS main.cpp importer.cpp nss.cpp dialog.cpp chromeimporter.cpp)

kde4_add_executable(importer ${SRCS})
target_link_libraries(importer ${KDE4_KDECORE_LIBS} ${KDE4_KPARTS_LIBS} ${KDE4_KDEUI_LIBS} ${QCA2_LIBRARIES} ${QT_QTSQL_LIBRARIES} -lnss3)
install(TARGETS importer ${INSTALL_TARGETS_DEFAULT_ARGS})


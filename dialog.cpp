#include "dialog.h"

#include <QLayout>
#include <QLabel>
#include <QPushButton>
#include <KDebug>
#include <QVBoxLayout>
#include <QSpacerItem>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent), m_chromeImporter(0)
{
    setWindowTitle("KWallet Password Importer");
    setLayout(new QVBoxLayout);
    m_firefoxImporter = new Importer;
    m_wallet = Wallet::openWallet(Wallet::NetworkWallet(),
                                  winId(),
                                  Wallet::Asynchronous);



    QLabel *explanation = new QLabel("<b>HELLO!</b><br/>"
                                     "Please click 'Start Import!' to start importing your <br/>"
                                     "login credentials from Chrome/Firefox to KWallet." );
    m_statusLabel = new QLabel("Opening wallet...", this);
    m_statusLabel->setAlignment(Qt::AlignCenter);
    m_progressBar = new QProgressBar(this);
    m_launchButton = new QPushButton("Start import!", this);
    m_launchButton->setDisabled(true);
    m_chromeCheck = new QCheckBox(this);
    m_chromeCheck->setText("Import from Chrome");
    m_firefoxCheck = new QCheckBox(this);
    m_firefoxCheck->setText("Import from Firefox");


    layout()->addWidget(explanation);
    layout()->addWidget(m_chromeCheck);
    layout()->addWidget(m_firefoxCheck);
    qobject_cast<QVBoxLayout*>(layout())->addStretch();
    layout()->addWidget(m_statusLabel);
    layout()->addWidget(m_progressBar);
    layout()->addWidget(m_launchButton);

    connect(m_launchButton, SIGNAL(clicked()), SLOT(doImport()));
    connect(m_wallet, SIGNAL(walletOpened(bool)), SLOT(walletOpened(bool)));
    setMinimumSize(500, 200);
}

void Dialog::walletOpened(bool ok)
{

    if (ok &&
        (m_wallet->hasFolder(KWallet::Wallet::FormDataFolder()) ||
        m_wallet->createFolder(KWallet::Wallet::FormDataFolder())) &&
        m_wallet->setFolder(KWallet::Wallet::FormDataFolder())) {
        m_launchButton->setDisabled(false);
        m_statusLabel->setText("Idle.");
        m_chromeImporter = new ChromeImporter(m_wallet, this);
    } else {
        m_statusLabel->setText("Error opening wallet!");
    }

}

void Dialog::doImport()
{
    m_statusLabel->setText("Fetching credentials...");
    m_progressBar->setValue(-1);
    QList<Login> logins;
    if (m_chromeCheck->isChecked()) {
        logins.append(m_chromeImporter->fetchLogins());
    }
    if (m_firefoxCheck) {
        m_firefoxImporter->fetchLogins();
    }
    kDebug() << "Saving " << logins.size() << " credentials...";
    m_wallet->setFolder(KWallet::Wallet::FormDataFolder());

    m_statusLabel->setText("Saving credentials...");
    m_progressBar->setMaximum(logins.size());
    int current = 0;
    foreach(Login login, logins) {
        m_progressBar->setValue(++current);
        if (!login.isValid()) continue;
        kDebug() << "Importing for tis url:" << login.url;
        if (m_wallet->writeMap(login.url + "#", login.fields)) kWarning() << "failed to write to wallet";
    }
    m_statusLabel->setText("Finished!");

}

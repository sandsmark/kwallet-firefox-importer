#ifndef CHROMEIMPORTER_H
#define CHROMEIMPORTER_H

#include <QObject>
#include <KWallet/Wallet>
#include "login.h"

using KWallet::Wallet;

class ChromeImporter : public QObject
{
    Q_OBJECT
public:
    explicit ChromeImporter(Wallet *wallet, QObject *parent = 0);

signals:

public slots:
    QList<Login> fetchLogins();

private:
    struct PickFaen {
        QByteArray buffer;
        int offset;
        int readInt();
    };

    void dumpFolder(QString folder);
    void parseEntry(const QByteArray &entry);
    Wallet *m_wallet;
    QList<Login> m_logins;
};

#endif // CHROMEIMPORTER_H

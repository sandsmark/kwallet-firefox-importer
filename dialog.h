#ifndef DIALOG_H
#define DIALOG_H

#include "importer.h"

#include "chromeimporter.h"
#include <QDialog>
#include <QLabel>
#include <QProgressBar>
#include <QCheckBox>
#include <KWallet/Wallet>

using KWallet::Wallet;

class Dialog : public QDialog
{
Q_OBJECT
public:
    Dialog(QWidget *parent = 0);

private slots:
    void doImport();
    void walletOpened(bool ok);

private:
    QCheckBox *m_chromeCheck;
    ChromeImporter *m_chromeImporter;
    QCheckBox *m_firefoxCheck;
    Importer *m_firefoxImporter;
    Wallet *m_wallet;
    QProgressBar *m_progressBar;
    QLabel *m_statusLabel;
    QPushButton *m_launchButton;
};

#endif // DIALOG_H

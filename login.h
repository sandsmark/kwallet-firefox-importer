#ifndef LOGIN_H
#define LOGIN_H

#include <QMap>

struct Login
{
    QString url;
    QMap<QString, QString> fields;
    bool isValid() {
        if (url.isEmpty()) return false;
        foreach(QString field, fields.keys())
            if (field.isEmpty() || fields[field].isEmpty()) return false;
        return true;
    }

};

#endif // LOGIN_H

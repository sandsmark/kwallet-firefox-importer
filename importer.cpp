#include "importer.h"
#include "nss.h"
#include <nss/nss.h>

#include <QSettings>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDir>
#include <KDebug>
#include <KWallet/Wallet>

Importer::Importer()
{
    QString firefoxPath = QDir::homePath().append("/.mozilla/firefox/");
    QSettings s(firefoxPath + "profiles.ini", QSettings::IniFormat);
    s.beginGroup("Profile0");
    QString profile = s.value("Path").toString();
    m_profilePath = firefoxPath.append(profile).toLocal8Bit();

    QByteArray NSSDir = QString(QDir::homePath() + "/.pki/nssdb/").toLocal8Bit();
    QByteArray defSpec("configDir='");
    defSpec.append(NSSDir);
    defSpec.append("' tokenDescription='Real NSS database'");
    NSSDir.prepend("sql:");
    NSS_Init(NSSDir);
    m_slot = SECMOD_OpenUserDB(defSpec.constData());
    Q_ASSERT(m_slot);

    if (PK11_NeedUserInit(m_slot))
        PK11_InitPin(m_slot, NULL, NULL);

    QByteArray spec("configDir='");
    spec.append(m_profilePath);
    spec.append("' tokenDescription='Firefox NSS database' flags=readOnly");
    m_slot = SECMOD_OpenUserDB(spec);
    Q_ASSERT(m_slot);
}

Importer::~Importer() 
{
    PK11_FreeSlot(m_slot);
}

QList<Login> Importer::fetchLogins()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    QString sqliteFile = m_profilePath + "/signons.sqlite";
    kDebug() << "Opening database file [" << sqliteFile << "]...";
    db.setDatabaseName(sqliteFile);
    if (!db.open())
        kFatal() << "FAILED TO OPEN THE GORRAM DATABASE";

    QSqlQuery query("SELECT formSubmitURL, usernameField, passwordField, encryptedUsername, encryptedPassword FROM moz_logins");

    QString usernameField, passwordField, username, password;
    QList<Login> logins;

    while (query.next()) {
        Login login;
        login.url = query.value(0).toString();
        usernameField = query.value(1).toString();
        passwordField = query.value(2).toString();
        username = query.value(3).toString();
        password = query.value(4).toString();
        login.fields[usernameField] = decrypt(username);
        login.fields[passwordField] = decrypt(password);
        logins.append(login);
    }

    return logins;
}

QString Importer::decrypt(QString input)
{
    QByteArray encrypted = QByteArray::fromBase64(input.toLocal8Bit());

    // Old style, only base64 encoded
    if (input[0] == '~') return encrypted;

    SECStatus result = PK11_Authenticate(m_slot, PR_TRUE, NULL);
    if (result != SECSuccess) {
        kFatal() << "Unable to authenticate"; 
        return QString();
    }

    SECItem request, reply;
    request.data = reinterpret_cast<unsigned char*>(encrypted.data());
    request.len = encrypted.size();
    reply.data = NULL;
    reply.len = 0;

    result = NSS::decrypt(m_slot, &request, &reply, NULL);
    //result = PK11SDR_Decrypt(&request, &reply, NULL); // shit no werk

    if (result != SECSuccess) {
        kWarning() << "unable to decrypt (" << result << ")";
        kDebug() << "this:" << input;
        return QString();
    }

    QString decrypted = QString::fromAscii(reinterpret_cast<char*>(reply.data), reply.len);

    return decrypted;
}

#include "chromeimporter.h"
#include <QDebug>
#include <QtCrypto/qca.h>
#include <inttypes.h>

static int read32bit(const QByteArray &buffer, int offset)
{
    int32_t r = *(reinterpret_cast<const int32_t*>(buffer.constData() + offset));
    return r;
}

static inline int align32(size_t offset)
{
    if (offset % 4)
        offset += (4 - (offset % 4));

    return offset;
}


static inline int align64(size_t offset)
{
    if (offset % 8)
        offset += (8 - (offset % 8));

    return offset;
}

ChromeImporter::ChromeImporter(Wallet *wallet, QObject *parent) :
    QObject(parent), m_wallet(wallet)
{
}

QList<Login> ChromeImporter::fetchLogins()
{
    foreach(const QString folder, m_wallet->folderList()) {
        if (folder.startsWith("Chrome Form Data")) {
            dumpFolder(folder);
        }
    }
    return m_logins;
}

void ChromeImporter::dumpFolder(QString folder)
{
    m_wallet->setFolder(folder);
    QByteArray buffer;

    foreach(const QString entry, m_wallet->entryList()) {
        if (m_wallet->entryType(entry) != KWallet::Wallet::Stream) continue;
        m_wallet->readEntry(entry, buffer);
        parseEntry(buffer);
    }
}


// TODO: abstract away the parsing of strings and shit to a separate function
void ChromeImporter::parseEntry(const QByteArray &entry)
{
    Login login;

    int urlLength = read32bit(entry, 20);
    login.url = QString::fromAscii(entry.mid(24, urlLength)); // origin
    int offset = align32(24 + urlLength);
    offset = align32(offset + 4 + read32bit(entry, offset));
    int length = read32bit(entry, offset);
    offset += 4;
    QString usernameField = QString::fromUtf16(reinterpret_cast<const ushort*>(entry.constData()+ offset), length);
    offset = align32(offset + length*2);
    length = read32bit(entry, offset);
    offset += 4;
    QString username = QString::fromUtf16(reinterpret_cast<const ushort*>(entry.constData()+ offset), length);
    offset = align32(offset + length*2);
    length = read32bit(entry, offset);
    offset += 4;

    login.fields[usernameField] = username;

    QString passwordField = QString::fromUtf16(reinterpret_cast<const ushort*>(entry.constData()+ offset), length);
    offset = align32(offset + length*2);
    length = read32bit(entry, offset);
    offset += 4;
    QString password = QString::fromUtf16(reinterpret_cast<const ushort*>(entry.constData()+ offset), length);

    login.fields[passwordField] = password;
    m_logins.append(login);
    //qDebug() << login.url << usernameField << username << passwordField << password;
}

